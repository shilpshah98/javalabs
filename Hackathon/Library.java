package Hackathon;

import java.util.HashSet;

public class Library {
    static HashSet<LibraryItem> inventory = new HashSet<LibraryItem>();

    public void addItem(LibraryItem item) {
        inventory.add(item);
        System.out.println("Added item: " + item.getTitle());
    }

    public void removeItem(LibraryItem item) {
        inventory.remove(item);
        System.out.println("Removed item: " + item.getTitle());
    }

    public void checkout(LibraryItem item) {
        if (!item.isBorrowable()) {
            System.out.println("Cannot checkout Periodicals\n");
        } else if (!item.isAvailable()) {
            System.out.println("Item not currently available\n");
        } else if (!inventory.contains(item))  {
            System.out.println("Item not in inventory\n");
        }
        else {
            item.setAvailable(false);
            System.out.println("Checking out item: " + item.getTitle());
        }
    }

    public static void main(String[] args) {

        Library lib = new Library();
        // Creating books
        Books book1 = new Books("AA", "Book1", ItemType.BOOK);
        Books book2 = new Books("BB", "Book2", ItemType.BOOK);

        lib.addItem(book1);
        lib.addItem(book2);

        // Creating CDs
        Cds cd1 = new Cds("CC", "CD1", ItemType.CDS);
        Cds cd2 = new Cds("DD", "CD2", ItemType.CDS);

        lib.addItem(cd1);
        lib.addItem(cd2);

        // Creating Dvds
        Dvds dvd1 = new Dvds("EE", "DVD1", ItemType.DVDS);
        Dvds dvd2 = new Dvds("FF", "DVD2", ItemType.DVDS);

        lib.addItem(dvd1);
        lib.addItem(dvd2);

        // Creating Periodicals
        Periodicals p1 = new Periodicals("GG", "Periodical1", ItemType.PERIODICALS);
        lib.addItem(p1);

        //Testing remove method
        lib.removeItem(book1);

        //Testing checkout method
        System.out.println("Trying to checkout Book1");
        lib.checkout(book1);

        System.out.println("Trying to checkout cd1");
        lib.checkout(cd1);

        System.out.println("Trying to checkout cd1");
        lib.checkout(cd1);
    }
}