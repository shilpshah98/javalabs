package Hackathon;

public abstract class LibraryItem {
    private String author;
    private String title;
    private boolean isBorrowable;
    private boolean isAvailable;
    private ItemType itemType;
    // private String user;

    public LibraryItem(String author, String title, ItemType itemType) {
        this.author = author;
        this.title = title;
        this.isAvailable = true;

        if (itemType == ItemType.PERIODICALS) {
            this.isBorrowable = false;
        }
        else {
            this.isBorrowable = true;
        }
        
    }
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBorrowable() {
        return isBorrowable;
    }

    public void setBorrowable(boolean isBorrowable) {
        this.isBorrowable = isBorrowable;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
    
    
}

