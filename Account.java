
public class Account implements Detailable {
    private double balance;
    private String name;
    private static double interestRate;
    
    public Account(String name, double balance) throws DodgyNameException {
        if ("Fingers".equals(name)) {
            throw new DodgyNameException();
        }
        else {
            this.name = name;
            this.balance = balance;

        }

    }

    public Account() throws DodgyNameException {
        new Account("Shilp", 50.0);
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException {
        if ("Fingers".equals(name)) {
            throw new DodgyNameException();
        }
        else {
            this.name = name;
        }
    }

    public void addInterest() {
        balance *= interestRate;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        if(this.getBalance() < amount) {
            System.out.println("Not enough balance");
            return false;
        }
        else {
            this.setBalance(this.getBalance() - amount);
            System.out.println("Balance withdrawn");
            return true;
        }
    }

    public boolean withdraw() {
        return this.withdraw(20);
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return "Name: " + this.getName() + " and balance: " + this.getBalance();
    }

}

