public class TestExceptions {
    public static void main(String[] args) {
        double[] amounts = { 23, 5444, 2, 345, 34 };
        String[] names = { "Picard", "Ryker", "Worf", "Fingers", "Data" };

        Account.setInterestRate(1.25);

        Account[] accounts = new Account[5];
        for (int i = 0; i < 5; i++) {
            try {
                accounts[i] = new Account(names[i], amounts[i]);
            } catch (DodgyNameException e) {
                e.toString();
                e.printStackTrace();
            }
            System.out.println("Old Balance: " + accounts[i].getBalance());
            accounts[i].addInterest();
            System.out.println("New Balance: " + accounts[i].getBalance());
        }
    }
}