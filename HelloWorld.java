
public class HelloWorld {

    public static void main(String[] args) {
        String make = "Test";
        String model = "TestModel";
        double engineSize = 1.8;
        byte gear = 2;
        short speed = (short) (gear * 20);


        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if (engineSize <= 1.3) {
            System.out.println("Engine is powerful for this car");
        }

        if (gear == 1) {
            System.out.println("Speed should be less than 10 mph");
        }
        else if (gear == 2) {
            System.out.println("Speed should be less than 20 mph");
        }
        else if (gear == 3) {
            System.out.println("Speed should be less than 30 mph");
        }

        

    }
}