import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class TestAccount {
    public static void main(String[] args) throws DodgyNameException {
        Account myAccount = new Account();
        myAccount.setName("Shilp");
        myAccount.setBalance(395828);

        myAccount.addInterest();
       // System.out.println("Account: " + myAccount.getName() + " has " + myAccount.getBalance() + " dollars");

        //Using Collections
        Account.setInterestRate(2.25);
        
        AccountComparator ac = new AccountComparator();
        TreeSet<Account> accounts = new TreeSet<>(ac);



        Account a1 = new Account("Shilp", 27842);
        Account a2 = new Account("Bob", 8432);
        Account a3 = new Account("John", 5948224);

        accounts.add(a1);
        accounts.add(a2);
        accounts.add(a3);

        System.out.println("Account order: ");
        accounts.forEach(a -> System.out.println("Name: " + a.getName()));

        System.out.println("Printing using Iterator");
        Iterator<Account> iter = accounts.iterator();
        while(iter.hasNext()) {
            Account a = iter.next();
            System.out.println("Name: " + a.getName() + " and balance is: " + a.getBalance());
            a.addInterest();
            System.out.println("new Balance is: " + a.getBalance());
        }

        System.out.println("\nPrinting using for loop");
        for (Account a: accounts) {
            System.out.println("Name: " + a.getName() + " and balance is: " + a.getBalance());
            a.addInterest();
            System.out.println("new Balance is: " + a.getBalance());
        }

        System.out.println("\nPrinting using forEach");
        accounts.forEach(a -> { 
            System.out.println("Name: " + a.getName() + " and balance is: " + a.getBalance());
            a.addInterest();
            System.out.println("new Balance is: " + a.getBalance());
        });


    }
}